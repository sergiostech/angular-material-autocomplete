import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { AutocompleteChipsComponent } from './autocomplete-chips/autocomplete-chips.component';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule, MatChipsModule, MatIconModule, MatInputModule, MatFormFieldModule } from '@angular/material';

describe('AppComponent', () => {
  let app: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  const mockNewValue = 'France';
  const mockOptions = [ 'Belgrade', 'Serbia', 'Europe' ];
  const mockPlaceHolder = 'Please enter data';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        AppRoutingModule,
        FormsModule,
        MatAutocompleteModule,
        MatChipsModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        NoopAnimationsModule,
        ReactiveFormsModule
      ],
      declarations: [
        AppComponent,
        AutocompleteChipsComponent,
        AutocompleteComponent,
      ],
      providers: [ {provide: APP_BASE_HREF, useValue: '/'} ]
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.debugElement.componentInstance;
  }));

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it('should test AutocompleteChipsComponent @input/output', () => {
    // Set options
    app.options = mockOptions;
    app.placeHolder = mockPlaceHolder;
    fixture.detectChanges();
    spyOn(app, 'onPayload');
    // Insert a new value
    const inputElement: HTMLInputElement = fixture.nativeElement.querySelector('#chips-input');
    expect(inputElement).not.toBeNull();
    inputElement.value = mockNewValue;
    inputElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const autocompleteElement: HTMLInputElement = fixture.nativeElement.querySelector(
      '#chips-autocomplete'
    );
    // Insert a new chips by pressing Enter key using already inserted value
    autocompleteElement.dispatchEvent(new KeyboardEvent('keydown', {
      bubbles: true,
      cancelable: true,
      key: 'Enter'
    }));
    inputElement.dispatchEvent(new Event('blur'));
    fixture.detectChanges();
    // Test if inserted value is propagated to the parent
    expect(app.onPayload).toHaveBeenCalledWith([mockNewValue]);
  });

});
