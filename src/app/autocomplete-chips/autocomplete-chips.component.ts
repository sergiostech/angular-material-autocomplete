import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import {
  MatAutocompleteSelectedEvent,
  MatAutocomplete
} from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';

@Component({
  selector: 'app-autocomplete-chips',
  templateUrl: './autocomplete-chips.component.html',
  styleUrls: ['./autocomplete-chips.component.scss']
})
export class AutocompleteChipsComponent {
  @Input() ariaLabel: string;
  @Input() options: string[];
  @Input() placeHolder: string;
  @Output() payload = new EventEmitter<string[]>();
  @ViewChild('chipInput', { static: false }) chipInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', { static: false }) matAutocomplete: MatAutocomplete;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  addOnBlur = true;
  autocompleteChipsControl = new FormControl();
  insertedItems: string[] = [];
  removable = true;
  selectable = true;
  setOfOptions: Observable<string[]>;

  constructor() {
    this.setOfOptions = this.autocompleteChipsControl.valueChanges.pipe(
      startWith(''),
      map(value => (value ? this.filterOptions(value) : this.options.slice()))
    );
  }

  /**
   * Add new item only when MatAutocomplete is not open
   * Prevent conflict with OptionSelected Event
   * @param {MatChipInputEvent} event
   * @returns void
   */
  addNew(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if (!this.matAutocomplete.isOpen && (value || '').trim()) {
      this.insert(value.trim());
    }
    if (input) {
      input.value = '';
    }
    this.autocompleteChipsControl.setValue(undefined);
  }

  /**
   * Add selected option from predefined input options
   * Only if it is not already in the array of inserted items, preventing insertion of items with same values
   * @param {MatAutocompleteSelectedEvent} event
   * @returns void
   */
  addSelected(event: MatAutocompleteSelectedEvent): void {
    const item = event.option.viewValue;
    if (!this.insertedItems.includes(item)) {
      this.insert(item);
    }
    this.chipInput.nativeElement.value = '';
    this.autocompleteChipsControl.setValue(undefined);
  }

  /**
   * Set focus to chip input element
   * @returns void
   */
  focusChipInput(): void {
    this.chipInput.nativeElement.blur();
    this.chipInput.nativeElement.focus();
  }

  /**
   * Remove an item from selected options array
   * @param {string} item
   * @returns void
   */
  remove(item: string): void {
    if (this.insertedItems.includes(item)) {
      this.insertedItems.splice(this.insertedItems.indexOf(item), 1);
      this.updatePayload();
    }
  }

  /**
   * Return filtered options by value
   * @param {string} value
   * @returns string[]
   */
  private filterOptions(value: string): string[] {
    return this.options.filter(
      (option) => option.toLowerCase().indexOf(value.toLocaleLowerCase()) === 0
    );
  }

  /**
   * Insert new item to the end of inserted items array
   * @param {string} item
   * @returns void
   */
  private insert(item: string): void {
    this.insertedItems.push(item);
    this.updatePayload();
  }

  /**
   * Updates the output payload by emitting an event containing inserted items array
   * @returns void
   */
  private updatePayload(): void {
    this.payload.emit(this.insertedItems);
  }
}
