import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatAutocompleteModule,
  MatChipsModule,
  MatIconModule,
  MatInputModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutocompleteChipsComponent } from './autocomplete-chips.component';

describe('AutocompleteChipsComponent', () => {
  let instance: AutocompleteChipsComponent;
  let fixture: ComponentFixture<AutocompleteChipsComponent>;
  let inputElement: HTMLInputElement;
  const mockNewValue = 'France';
  const mockOptions = [ 'Belgrade', 'Serbia', 'Europe' ];
  const mockPlaceHolder = 'Please enter data';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AutocompleteChipsComponent
      ],
      imports: [
        FormsModule,
        MatAutocompleteModule,
        MatChipsModule,
        MatIconModule,
        MatInputModule,
        NoopAnimationsModule,
        ReactiveFormsModule
      ],
    })
    .compileComponents();
    fixture = TestBed.createComponent(AutocompleteChipsComponent);
    instance = fixture.componentInstance;
    instance.options = mockOptions;
    instance.placeHolder = mockPlaceHolder;
    fixture.detectChanges();
    inputElement = fixture.nativeElement.querySelector('#chips-input');
    spyOn(instance.payload, 'emit');
  }));

  it('should create', () => {
    expect(instance).toBeTruthy();
  });

  it('should accept a new value', () => {
    expect(inputElement).not.toBeNull();
    inputElement.value = mockNewValue;
    inputElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(instance.chipInput.nativeElement.value).toBe(mockNewValue);
  });

  it('should received @input options', () => {
    instance.setOfOptions.subscribe(result => {
      expect(result).toEqual(mockOptions);
    });
  });

  it('should emit newly inserted values after COMMA key is pressed to @output', () => {
    expect(inputElement).not.toBeNull();
    inputElement.value = mockNewValue;
    inputElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const autocompleteElement: HTMLInputElement = fixture.nativeElement.querySelector(
      '#chips-autocomplete'
    );
    autocompleteElement.dispatchEvent(new KeyboardEvent('keydown', {
      bubbles: true,
      cancelable: true,
      key: ','
    }));
    inputElement.dispatchEvent(new Event('blur'));
    fixture.detectChanges();
    expect(instance.insertedItems).toEqual([mockNewValue]);
    expect(instance.payload.emit).toHaveBeenCalledWith([mockNewValue]);
  });

  it('should remove inserted values', () => {
    instance.insertedItems = [mockNewValue];
    instance.remove(mockNewValue);
    expect(instance.insertedItems).toEqual([]);
  });

});
