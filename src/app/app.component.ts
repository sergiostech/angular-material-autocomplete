import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  options: string[] = [
    'Belgrade',
    'Serbia',
    'Europe'
  ];
  label = 'Items';
  placeHolder = 'Please insert items';

  onPayload(data: any) {
    console.log(data);
  }

}
