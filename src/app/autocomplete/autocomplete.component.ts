import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements OnInit {
  @Input() ariaLabel: string;
  @Input() options: string[];
  @Input() placeHolder: string;
  @Output() payload = new EventEmitter<string>();
  autocompleteControl = new FormControl();
  setOfOptions: Observable<string[]>;

  constructor() { }

  ngOnInit() {
    this.setOfOptions = this.autocompleteControl.valueChanges
      .pipe(
        startWith(''),
        map((value) => value ? this.filterData(value) : this.options.slice())
      );
  }

  displayData(data: string): string | undefined {
    return data ? data : undefined;
  }

  /**
   * Updates the output payload by emitting an event containing a value
   * @returns void
   */
  updatePayload(value: string): void {
    this.payload.emit(value);
  }

  /**
   * Return filtered options by value
   * @param {string} value
   * @returns string[]
   */
  private filterData(value: string): string[] {
    return this.options.filter((option) => option.toLowerCase().indexOf(value.toLocaleLowerCase()) === 0);
  }

}
